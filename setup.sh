#to run use this command: curl -o- -H 'Cache-Control: no-cache' https://gitlab.com/firalogin/fira-backend-setup/-/raw/master/setup.sh | bash

sudo apt-get update
sudo apt-get -y upgrade

# Update NodeJS list
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -

# this command will download install script with curl
#curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

#export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# check if installed
#nvm --version

# list available Node.js versions
#nvm ls-remote

# choose one version and install it
# "node" is an alias for the latest version
#nvm install node

# Can use on ipv6 but cant install from NPM
sudo apt -y install nodejs
sudo apt -y install npm

# check if installed properly
nodejs -v

# AD NPM IPv6 addresses
echo '2606:4700::6810:1723 registry.npmjs.org' >> /etc/hosts
echo '2606:4700::6810:1723 registry.yarnpkg.com' >> /etc/hosts
echo '2606:4700::6810:ab63 yarnpkg.com' >> /etc/hosts

# go to your user's directory
cd /root

# create folder nodeserver
mkdir nodeserver

# go inside
cd nodeserver

# Download Server File

curl https://gitlab.com/firalogin/fira-backend-setup/-/raw/master/nodeserver/server.js --output server.js

# create package.json
npm init -y

# install Express.js
npm install express

# Allow to run with command line not open

# globally install PM2
npm install pm2 -g

# check if installed properly
pm2 -V

pm2 start server.js

pm2 startup systemd
